FROM cassandra:3.11.0
# Install Java Native Access
RUN apt-get update && apt-get install -y \
  libjna-java

# Setup database for Kaa
COPY ./files/cassandra.cql /var/lib/cassandra
# Environmental variables
COPY start.sh /var/
RUN chmod 777 /var/start.sh
# Open ports
EXPOSE 7000 7001 7199 9042 9160
# How to run after cassandra is running?
# cqlsh -f /var/lib/cassandra/cassandra.cql
