#!/bin/bash

sed -i "s/rpc_address: .*/rpc_address: $(hostname --ip-address)/" /etc/cassandra/cassandra.yaml
sed -i "s/listen_address: .*/listen_address: $(hostname --ip-address)/" /etc/cassandra/cassandra.yaml
sed -i "s/# broadcast_address: .*/broadcast_address: $(hostname --ip-address)/" /etc/cassandra/cassandra.yaml
sed -i "s/# broadcast_rpc_address: .*/broadcast_rpc_address: $(hostname --ip-address)/" /etc/cassandra/cassandra.yaml
sed -i "s/start_rpc: .*/start_rpc: true/" /etc/cassandra/cassandra.yaml
sed -i "s/- seeds: .*/- seeds: '$(hostname --ip-address)'/" /etc/cassandra/cassandra.yaml
cassandra -R
sleep 60

cqlsh -f /var/lib/cassandra/cassandra.cql $(hostname --ip-address)
while :
do
sleep 1
done